﻿using System;
using System.IO;

namespace desafio
{
    public class JogoDaVida
    {
        public char[,] dados = new char[5, 5];

        public JogoDaVida Evoluir()
        {
            var dadosAux = new char[5, 5];
            try
            {
                for (int i = 0; i < 5; i++)
                {
                    for (int j = 0; j < 5; j++)
                    {
                        int count = QtdCelulasVivasVizinhas(i, j);

                        if (count < 2)
                            dadosAux[i, j] = 'M';

                        if (count > 3)
                            dadosAux[i, j] = 'M';

                        if ((count == 2 || count == 3) && dados[i, j] == 'V')
                            dadosAux[i, j] = 'V';
                        else
                            dadosAux[i, j] = 'M';

                        if (count == 3 && dados[i, j] == 'M')
                            dadosAux[i, j] = 'V';
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            var newJogo = new JogoDaVida();
            newJogo.dados = dadosAux;
            return newJogo;
        }


        private int QtdCelulasVivasVizinhas(int x, int y)
        {
            try
            {
                if (!VerifyIfIsValid())
                    throw new Exception("Nao é uma Matriz valida.");

                int celulasVizinhas = 0;

                for (int i = 0; i < 5; i++)
                {
                    for (int j = 0; j < 5; j++)
                    {
                        if (i == x && j == y)
                        {
                            if (j > 0 && dados[i, j - 1] == 'V')
                                celulasVizinhas++;

                            if (j < 4 && dados[i, j + 1] == 'V')
                                celulasVizinhas++;

                            if (i > 0)
                            {
                                if (dados[i - 1, j] == 'V')
                                    celulasVizinhas++;

                                if (j > 0 && dados[i - 1, j - 1] == 'V')
                                    celulasVizinhas++;

                                if (j < 4 && dados[i - 1, j + 1] == 'V')
                                    celulasVizinhas++;
                            }

                            if (i < 4)
                            {
                                if (dados[i + 1, j] == 'V')
                                    celulasVizinhas++;

                                if (j > 0 && dados[i + 1, j - 1] == 'V')
                                    celulasVizinhas++;

                                if (j < 4 && dados[i + 1, j + 1] == 'V')
                                    celulasVizinhas++;
                            }
                        }
                    }
                }
                return celulasVizinhas;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void WriteObject(string fileToSave)
        {
            try
            {
                using (var streamWriter = new StreamWriter(@fileToSave))
                {
                    var json = Newtonsoft.Json.JsonConvert.SerializeObject(dados);
                    streamWriter.Write(json);
                }
            }catch(Exception ex)
            {
                throw new Exception("Houve falhas ao salvar o Arquivo, verifique o caminho para salvar o mesmo.");
            }
        }

        public void ReadFile(string fileName)
        {
            try
            {
                using (var file = new StreamReader(@fileName))
                {
                    var json01 = file.ReadToEnd();

                    dados = Newtonsoft.Json.JsonConvert.DeserializeObject<char[,]>(json01);
                }

                if (!VerifyIfIsValid())
                    throw new Exception("Arquivo não contém uma matriz Válida.");
            }
            catch
            {
                throw new Exception("O arquivo contem uma matriz invalida para o processo.");
            }
        }

        private bool VerifyIfIsValid()
        {
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    var caractere = Char.ToUpper(dados[i, j]);
                    if (caractere == ' ' || (caractere != 'V' && caractere != 'M'))
                        return false;
                }
            }
            return true;
        }
    }
}
