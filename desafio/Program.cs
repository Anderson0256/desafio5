﻿using System;
using System.IO;

namespace desafio
{
    public class Program
    {
        static void Main(string[] args)
        {
            try
            {
                JogoDaVida jogo01 = new JogoDaVida();

                Console.Write("Nome do Arquivo de Entrada: ");
                string arquivo = Console.ReadLine();

                if (string.IsNullOrWhiteSpace(arquivo.Trim()))
                    throw new Exception("Nome do Arquivo não pode ser Vazio.");

                if (!File.Exists(arquivo))
                    throw new Exception("Arquivo não encontrado.");

                jogo01.ReadFile(arquivo);

                Console.Write("Nome do arquivo a ser salvo: ");
                string fileToSave = Console.ReadLine();

                if (string.IsNullOrWhiteSpace(fileToSave.Trim()))
                    throw new Exception("Nome do arquiv a ser salvo nao pode ser vazio.");
                
                int count = 0;
                int mediaCelulasVivas = 0;
                int rodadaMaiorIndiceIteracao = 0;
                int rodadaMenorIndiceIteracao = 0;
                int celulasVivas = 0;
                int maiorQtdCelulasVivas = 0;
                int menorQtdCelulasVivas = 0;
                while (count < 4)
                {
                    jogo01 = jogo01.Evoluir();
                    celulasVivas = 0;

                    Console.WriteLine("Rodada {0}\n", count + 1);
                    for (int i = 0; i < 5; i++)
                    {
                        for (int j = 0; j < 5; j++)
                        {
                            Console.Write(" {0} |", jogo01.dados[i, j]);
                            if (jogo01.dados[i, j] == 'V')
                                celulasVivas++;
                        }
                        Console.Write("\n");
                    }
                    mediaCelulasVivas += celulasVivas;

                    if (count == 0)
                    {
                        maiorQtdCelulasVivas = celulasVivas;
                        menorQtdCelulasVivas = celulasVivas;
                    }
                    if (celulasVivas > maiorQtdCelulasVivas)
                    {
                        rodadaMenorIndiceIteracao = count;
                        maiorQtdCelulasVivas = celulasVivas;
                    }

                    if (celulasVivas <= menorQtdCelulasVivas)
                    {
                        rodadaMaiorIndiceIteracao = count;
                        menorQtdCelulasVivas = celulasVivas;
                    }

                    Console.WriteLine("Quantidade de Células Vivas: {0}\nQuantidade de Células Mortas: {1}\n", celulasVivas, jogo01.dados.Length - celulasVivas);
                    count++;
                }

                Console.WriteLine("Rodada com Menor Indice de Iteracao e maior quantidade de Celulas Vivas: {0} - {1}", rodadaMenorIndiceIteracao + 1, maiorQtdCelulasVivas);
                Console.WriteLine("Rodada com Maior Indice de Iteracao e menor quantidade de Celulas Vivas: {0} - {1}", rodadaMaiorIndiceIteracao + 1, menorQtdCelulasVivas);
                Console.WriteLine("Média de Células Vivas de todas as rodadas: {0}.", mediaCelulasVivas / count);

                jogo01.WriteObject(fileToSave);
                Console.WriteLine("\nArquivo salvo com Sucesso.\n\nCaminho do arquivo: {0}", fileToSave);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.ReadKey();
        }
    }
}
